var validar = function () {
    var nombres = document.formulario.nombre1;
    if (nombres.value == "") {
        document.getElementById("mensaje_nombres").innerText = "EL CAMPO ES REQUERIDO";
    } else if (nombres.value.length < 3) {
        document.getElementById("mensaje_nombres").innerText = "EL CAMPO DEBE CONTENER MINIMO 3 CARACTERES";
    } else if (!nombres.value.match(/^[a-z]+$/i)) {
        document.getElementById("mensaje_nombres").innerText = "EL CAMPO SOLO ACEPTA LETRAS";
    } else {
        document.getElementById("mensaje_nombres").innerText = "";
    }
}
var validardatos = function () {
    var email = document.formulario.email;
    if (email.value == "") {
        document.getElementById("mensaje_email").innerText = "EL CAMPO ES OBLIGATORIO";
    } else {
        document.getElementById("mensaje_email").innerText = "";
    }
}
var validarclave = function () {
    var pass = document.formulario.password1
    if (pass.value == "") {
        document.getElementById("mensaje_password").innerText = "EL CAMPO ES OBLIGATORIO";
    } else if (pass.value.length < 3) {
        document.getElementById("mensaje_password").innerText = "EL CAMPO DEBE CONTENER MINIMO 3 CARACTERES";
    } else if (!pass.value.match(/^[a-z]+$/i)) {
        document.getElementById("mensaje_password").innerText = "EL CAMPO SOLO ACEPTA LETRAS";
    } else {
        document.getElementById("mensaje_password").innerText = "";
    }
}
var validarclave2= function () {
    var pass2 = document.formulario.password2
    if (pass2.value == "") {
        document.getElementById("mensaje_password2").innerText = "EL CAMPO ES OBLIGATORIO";
    } else if (pass2.value.length < 3) {
        document.getElementById("mensaje_password2").innerText = "EL CAMPO DEBE CONTENER MINIMO 3 CARACTERES";
    } else if (!pass2.value.match(/^[a-z]+$/i)) {
        document.getElementById("mensaje_password2").innerText = "EL CAMPO SOLO ACEPTA LETRAS";
    } else {
        document.getElementById("mensaje_password2").innerText = "";
    }
}
    window.onload = function () {
        var boton = document.getElementById("btn_validar");
        boton.onclick = function () {
            validar();
            validardatos();
            validarclave();
            validarclave2();
        }
    }